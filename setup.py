#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='django-socialfeed',
    version='0.1',
    description='',
    author='Fabian Germann',
    author_email='fg@feinheit.ch',
    url='feinheit.ch',
    packages=find_packages()
)
